CREATE DATABASE agenda;

USE agenda;

CREATE TABLE contatos(
	id INTEGER AUTO_INCREMENT NOT NULL,
    nome VARCHAR(60),
    apelido VARCHAR(45),
    email VARCHAR(60),
    dt_nascimento VARCHAR(50),
    telefone VARCHAR(20),
    categoria VARCHAR(60),
    PRIMARY KEY (id)
);